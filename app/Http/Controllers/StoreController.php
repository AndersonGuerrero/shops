<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\Image;



class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    /** 
    *   $store = \App\Store::orderBy('created_at','desc')->paginate(5);
    *  $image = \App\Image::orderBy('created_at','desc')->paginate(5);
    * return view('admin.register.index',[
    *      'store'=>$store,
    *      'image'=>$image,
    *   ]);
    */

        $store = \App\Store::all();
        return $store;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
      /*---------------------------------------------------------------------------------------------*/

        /*  $input['nombre'] = $request->input('nombre'); 
            $input['direccion'] = $request->input('direccion'); 
            $input['telefono'] = $request->input('telefono'); 
            $input['email'] = $request->input('email');
            $input['mapa'] = $request->input('mapa'); 
            
            
            $input['pinture'] = null;

            if ($request->file('pinture')==null)
            {
                $input['pinture'] = null;
            }
            else
            {
                $image = $request->file('pinture');
                // almacena y captura el nombre del archivo
                $input['pinture'] =  $image->store('PicShops','public');
            }

            $data = Store::insert($input);

            $response['message'] = "Guardo exitosamente ";
            $response['success'] = true;
            return $response;*/

    /**------------------------------------------------------------------- */

            $storeShops = \App\Store::create([
                'nombre'=> $request->input('nombre'),
                'direccion'=> $request->input('direccion'),
                'telefono'=> $request->input('telefono'),
                'email'=> $request->input('email'),
                'mapa'=> $request->input('mapa'),
                'pinture'=>''
            ]);

           

            $picShop = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('tiendas/'.$storeShops->id.'/'), $picShop);
            \App\Store::find($storeShops->id)->update([
                'pinture'=>$picShop
            ]);

            return $response;

            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
