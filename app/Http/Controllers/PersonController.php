<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Avatar;
use Illuminate\Support\Facades\Validator;

class PersonController extends Controller
{


    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'nombre' => ['required', 'string','min:5', 'max:20'],
    //         'apellido' => ['required', 'string','min:5', 'max:20'],
    //         'cedula' => ['required', 'number', 'min:7', 'max:9', 'unique:users'],
    //         'nacimiento' => ['required', 'number'],
    //         'rol' => ['required', 'number'],

    //     ]);
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     */
    public function index()
    {
        $user = \App\Person::orderBy('created_at','desc')->paginate(5);
        $avatar = \App\Avatar::orderBy('created_at','desc')->paginate(5);
        return view('admin.register.index',[
            'user'=>$user,
            'avatar'=>$avatar,
        ]);
    }


    public function login()
    {
       
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.register.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $avatar = \App\Avatar::create([
            'imagen'=>''
        ]);
        $userAvatar = time().'.'.$request->imagen->getClientOriginalExtension();
        $request->imagen->move(public_path('avatars/'.$avatar->id.'/'), $userAvatar);
        \App\Avatar::find($avatar->id)->update([
            'imagen'=>$userAvatar
        ]);
    
        
        $person = \App\Person::create([
            'nombre'=> $request->input('nombre'),
            'apellido'=> $request->input('apellido'),
            'cedula'=> $request->input('cedula'),
            'nacimiento'=> $request->input('nacimiento'),
            'rol'=> $request->input('rol'),
            'imagen_id'=> $avatar->id,
            ]);
           
            return redirect('admin/register/');
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function delete(Request $request, $id){
        \App\Person::find($id)->delete();
        \App\Avatar::find($id)->delete();
        return redirect('admin/register/');
    }
}
