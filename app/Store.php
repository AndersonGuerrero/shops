<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;

class Store extends Model
{
    protected $fillable = [ 'nombre', 'direccion', 'telefono', 'email', 'mapa', 'pinture'];  
    public $timestamps = false; 

   /* public function image(){
         return $this->hasOne(Image::class);
    }*/
}
