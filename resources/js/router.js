import Vue from "vue";
import Router from "vue-router";
import Vuetify from "vuetify";

Vue.use(Router);
Vue.use(Vuetify);

export default new Router({
    routes: [
        {
            path: "/",
            name: "maps",
            component: require("./views/admin/maps.vue").default
        },
        {
            path: "/burguer",
            name: "burguer",
            component: require("./views/admin/burguer.vue").default
        },
        {
            path: "/shops",
            name: "shops",
            component: require("./views/admin/newShops.vue").default
        },
        {
            path: "/indi",
            name: "indi",
            component: require("./views/admin/indi.vue").default
        },
        {
            path: "/file",
            name: "file",
            component: require("./views/admin/file.vue").default
        }
    ],
    mode: "history"
});
