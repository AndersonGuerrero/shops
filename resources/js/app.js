/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');



Vue.component('conten-component', require('./components/app.component.vue').default);




import router from './router.js';
import Vuetify from 'vuetify';
import '../../node_modules/vuetify/dist/vuetify.min';
import Axios from 'axios'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'



Vue.config.productionTip = false
Vue.use(Vuetify)
export default new Vuetify({ })


const app = new Vue({
    el: '#main',
    vuetify: new Vuetify(),
    Axios,
    router,
});

